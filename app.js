const WebSocket = require("@discordjs/uws");
const msgpack = new (require("msgpack5"))();
const snekfetch = require("snekfetch");
const r = require("./src/rethink");
const {checkLogin} = require("./src/token");
const ops = require("./src/ops");
const env = require("./src/env");
const randomBytes = require("util").promisify(require("crypto").randomBytes);
const bigNumber = require("bignum");
const utils = require("./src/utils");
const constants = require("./src/constants");

const mongo = require("./src/mongo");

const nonces = new Map();

const wss = new WebSocket.Server({
  port: env.wsPort
});

require("http")
  .createServer(function(req, res) {
    console.log("Request opened");
    let reqBody = "";
    req.on("data", function(data) {
      reqBody += data;
    });
    req.on("end", async function() {
      if (!reqBody) return;
      try {
        var body = JSON.parse(reqBody);
      } catch (err) {
        console.error(reqBody, err);
        return;
      }
      const events = Array.isArray(body.d) ? body.d : [body.d];

      const promises = new Set();
      const unsubscriptions = new Set();

      for (const event of events) {
        console.log("event", event);
        const nonce = event.d.nonce;
        if (event.t == "MESSAGE_CREATE" && event.d.nonce) {
          delete event.d.nonce;
          event.d.nonce = undefined;
          delete event.d.nonce;
        }
        const encoded = msgpack.encode({
          o: ops.dispatch,
          d: event
        });
        for (const sessionId of body.r) {
          const session = sessions.get(sessionId);
          if (body.r._n) {
            const old = nonces.get(body.r._n);
            if (old) {
              if (old.has(sessionId)) {
                // Just ignore the dupes
                return;
              }
            } else {
              nonces.set(body.r._n, new Set([sessionId]));
            }
          }
          if (session) {
            if (
              event.t == "MESSAGE_CREATE" &&
              session.userId == event.d.authorId &&
              nonce
            ) {
              const encodedNonce = msgpack.encode({
                o: ops.dispatch,
                // Not happy about that, but it's alright
                d: Object.assign({nonce}, event)
              });
              sendFrame(session.ws, encodedNonce, true);
            } else {
              sendFrame(session.ws, encoded, true);
            }
          } else {
            console.log(
              `Unsubscribing from non-existent session: ${sessionId}`
            );
            const sessionIndex = body.r.indexOf(sessionId);
            if (sessionIndex != -1) body.r.splice(sessionIndex, 1);
            else console.log("How did we delete ourselves?");
            unsubscriptions.add(sessionId);
          }
        }
      }
      if (unsubscriptions.size) {
        if (unsubscriptions.size == 1) {
          const sessionId = unsubscriptions.values().next().value;
          promises.add(
            mongo.db.collection("subscriptions").deleteManyAsync({
              sessionId
            })
          );
          promises.add(
            mongo.db.collection("sessions").deleteOneAsync({
              sessionId
            })
          );
        } else {
          const sessions = Array.from(unsubscriptions.values());
          promises.add(
            mongo.db.collection("subscriptions").deleteManyAsync({
              sessionId: {$in: sessions}
            })
          );
          promises.add(
            mongo.db.collection("sessions").deleteManyAsync({
              sessionId: {$in: sessions}
            })
          );
        }
      }
      if (promises.size) {
        console.log(promises.size, promises);
        await Promise.all(promises.values());
      }

      res.writeHead(204);
      res.end("");
    });
  })
  .listen(env.httpPort);

const sessions = new Map();

async function checkToken(token) {
  const userId = token.substring(0, token.indexOf("."));
  const user = await r
    .table("users")
    .get(userId)
    .run(r.conn);
  if (!user) return null;
  if (checkLogin(token, user.passwordHash, userId)) return userId;
  else return null;
}

function close(sessionId, code) {
  console.log(
    "CLOSING SESSION...",
    sessionId,
    "Why?",
    code,
    new Error("nab").stack
  );
  const session = sessions.get(sessionId);
  session.killing = true;
  Promise.all([
    mongo.db
      .collection("sessions")
      .findOne({
        userId: session.userId,
        sessionId: {$not: {$eq: sessionId}}
      })
      .then(
        sessionDocument =>
          sessionDocument ||
          r
            .table("users")
            .get(session.userId)
            .update({presence: constants.presence.offline})
            .run(r.conn)
      ),
    mongo.db.collection("sessions").deleteOneAsync({sessionId}),
    mongo.db.collection("subscriptions").deleteManyAsync({sessionId})
  ])
    .then(result => {
      console.log("Disconnected session", sessionId);
    })
    .catch(err => {
      console.error("Error disconnecting session", err);
      collectError(err);
    });
  if (code !== null) {
    try {
      sendOp(sessionId, "close", {code});
    } catch (err) {
      console.log("close sent bad", err);
    }
  }
  clearInterval(session.pingInterval);
  if (session.killTimeout !== null) clearTimeout(session.killTimeout);
  if (code !== null) {
    session.ws.close(code);
  }
  sessions.delete(sessionId);
}

async function onMessage(sessionId, packet) {
  try {
    var msg = msgpack.decode(packet);
  } catch (err) {
    return close(sessionId, 4101);
  }
  console.log(sessionId, "<-", msg);
  switch (msg.o) {
    case ops.subscribe: {
      // TODO: Permission checks
      const {userId} = sessions.get(sessionId);

      const documents = [];
      const queries = [];
      const total = {};
      for (const subscriptionKey in msg.d._) {
        total[subscriptionKey] = {};
        for (const itemId of msg.d._[subscriptionKey]) {
          documents.push({
            subscriptionQuery: subscriptionKey + "_" + itemId,
            hostAddress: env.hostAddress,
            userId,
            sessionId,
            active: true,
            lastId: null
          });
          queries.push(subscriptionKey + "_" + itemId);
        }
      }

      console.log("existingSubscriptions");

      const existingSubscriptions = await mongo.db
        .collection("subscriptions")
        .findAsync({
          sessionId,
          subscriptionQuery: {
            $in: queries
          }
        });

      console.log("existingSubscriptions $");

      const subscriptions = [];

      for await (const subscription of existingSubscriptions) {
        if (!subscription.active && subscription.lastId)
          subscriptions.push({
            query: subscription.subscriptionQuery,
            lastId: subscription.lastId
          });

        const subscriptionKey = subscription.subscriptionQuery.substring(0, 1);
        const itemId = subscription.subscriptionQuery.substring(2);

        if (!total[subscriptionKey]) {
          console.log("No?", subscription, msg.d._);
          total[subscriptionKey] = {
            [itemId]: true
          };
        } else {
          total[subscriptionKey][itemId] = true;
        }
      }

      if (subscriptions.length) {
        console.log("eventBacklog", subscriptions);

        const eventBacklog = await mongo.db.collection("events").findAsync({
          $or: subscriptions.map(({query, lastId}) => ({
            subscriptionQuery: query,
            dispatchId: {
              $gt: lastId
            }
          }))
        });

        console.log("eventBacklog $");

        for await (const event of eventBacklog) {
          console.log("unwinding; dispatching:", event);
          sendOp(sessionId, "dispatch", {
            t: event.eventType + "_" + event.eventAction,
            d: event.payload
          });
        }
      }

      await mongo.db.collection("subscriptions").insertManyAsync(documents);

      sendOp(sessionId, "subscribe", {
        d: total,
        n: msg.d.n === undefined ? null : msg.d.n
      });
      break;
    }
    case ops.unsubscribe: {
      console.log("Unsubscribing", msg.d._);
      const {userId} = sessions.get(sessionId);
      const promises = new Set();
      const lastEvent = await mongo.db.collection("events").findOneAsync();
      const lastId = (lastEvent && lastEvent.dispatchId) || "0";

      const documents = [];

      for (const subscriptionKey in msg.d._) {
        for (const itemId of msg.d._[subscriptionKey]) {
          documents.push(subscriptionKey + "_" + itemId);
        }
      }

      await mongo.db.collection("subscriptions").updateManyAsync(
        {sessionId, subscriptionQuery: {$in: documents}},
        {
          $set: {
            active: false,
            lastId
          }
        }
      );

      sendOp(sessionId, "unsubscribe", {n: msg.d.n || null});
      break;
    }
    case ops.heartbeat: {
      const session = sessions.get(sessionId);
      if (session.killTimeout !== null) {
        clearTimeout(session.killTimeout);
        session.killTimeout = null;
      }
      break;
    }
    case ops.presence: {
      const {userId} = sessions.get(sessionId);
      const presence = constants.presenceIds.has(msg.d.presence)
        ? msg.d.presence
        : constants.presence.offline;
      const activity =
        typeof msg.d.activity == "string" ? msg.d.activity : null;
      await utils.setPresence({
        userId,
        presence,
        activity
      });
      // We don't have any real response here because you get a USER_UPDATE event so it doesn't really matter
      break;
    }
    case ops.rtc: {
      const channelId = msg.d.c;
      const rtcServer = await utils.getRTCAddress(channelId);
      const session = sessions.get(sessionId);
      session.rtcServers.set(channelId, rtcServer);
      console.log(
        "Putting private rtcServer...",
        `http://${rtcServer.private}:3009/rooms/${channelId}/peers/${
          session.userId
        }`
      );
      try {
        await snekfetch.put(
          `http://${rtcServer.private}:3009/rooms/${channelId}/peers/${
            session.userId
          }`
        );
      } catch (err) {
        collectError(err);
        return close(sessionId, 4500);
      }
      console.log("Done, added");
      sendOp(sessionId, "rtc", {
        c: channelId,
        s: `ws${env.secure ? "s" : ""}://${rtcServer.public}`,
        n: msg.d.n
      });
      break;
    }
    default: {
      // Unknown OP
      close(sessionId, 4102);
    }
  }
}

function sendOp(sessionId, op, data) {
  const session = sessions.get(sessionId);
  if (session) {
    return sendFrame(session.ws, {
      o: ops[op],
      d: data
    });
  }
}

function sendFrame(ws, msg, noEncode) {
  console.log(ws.__sessionId, "->", msg);
  if (!noEncode) {
    msg = msgpack.encode(msg);
  }
  return ws.send(msg);
}

wss.on("connection", function(ws) {
  ws.once("message", async function(packet) {
    try {
      var userId = await checkToken(packet);
    } catch (err) {
      return ws.close(4500);
    }
    if (userId === null) {
      return ws.close();
    }
    const sessionId = (await randomBytes(16)).toString("base64");
    ws.__sessionId = sessionId;

    const sessionObject = {
      userId,
      ws,
      rtcServers: new Map(),
      pingInterval: setInterval(() => {
        sendFrame(ws, {
          o: ops.heartbeat
        });
        // Otherwise we end up orphaning timeouts
        if (!sessionObject.killTimeout) {
          sessionObject.killTimeout = setTimeout(() => {
            console.log("killTimeout ticked, killing session");
            close(sessionId, 4103);
          }, 10000);
        }
      }, 5000),
      killTimeout: null
    };

    sessions.set(sessionId, sessionObject);
    ws.send(
      msgpack.encode({
        o: ops.hello,
        d: {
          s: sessionId,
          h: 5000
          // what do we even put in here...
        }
      })
    );
    ws.on("message", onMessage.bind(null, sessionId));
    ws.on("close", function() {
      console.log("ws appears to have closed...");
      if (sessions.has(sessionId) && !sessions.get(sessionId).killing) {
        close(sessionId, null);
      } else {
        console.log("session is not intact");
      }
    });

    await Promise.all([
      mongo.db.collection("sessions").insertOneAsync({
        sessionId,
        userId,
        hostAddress: env.hostAddress
      }),
      utils.setPresence({
        userId,
        activity: null,
        presence: constants.presence.online
      })
    ]);
  });
});

function collectError(err) {
  if (env.deployment == "production") {
    console.error("Collected error, reporting (noop!!!)", err);
  } else {
    console.error("Collected error, crashing!", err);
    throw err;
  }
}

process.on("unhandledRejection", function(err) {
  throw err;
});
