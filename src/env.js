module.exports = {
  deployment: process.env.NODE_ENV || "local",
  secure: !!process.env.DOCKER,

  rtcServerMap: [
    {
      private: process.env.RTC_PRIVATE,
      public: process.env.RTC_PUBLIC || "harmonica.gq/rtc"
    }
  ],
  mongo: {
    db: process.env.MONGO_DB || "harmony-state",
    host: process.env.MONGO_HOST || "localhost",
    port: process.env.MONGO_PORT || "27017"
  },

  rethink: {
    host: process.env.RETHINK_HOST || "localhost",
    port: process.env.RETHINK_PORT || 28015,
    user: process.env.RETHINK_USER || "admin",
    password: process.env.RETHINK_PASSWORD || "",
    get db() {
      return `harmony_${module.exports.deployment}`;
    }
  },
  serverSalt: process.env.SERVER_SALT || "salt",
  epoch: (process.env.EPOCH
    ? new Date(Date.UTC(process.env.EPOCH))
    : new Date(Date.UTC(2018, 1, 1))
  ).getTime(),
  hostAddress: process.env.HTTP_ADDRESS || "http://localhost:3006",
  wsPort: process.env.WS_PORT || 3005,
  httpPort: process.env.HTTP_PORT || 3006
};

if (process.env.DOCKER)
  module.exports.hostAddress = `http://${require("os").hostname()}:${
    module.exports.httpPort
  }`;
