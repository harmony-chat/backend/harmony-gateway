module.exports = {
  presence: {
    offline: 0,
    online: 1,
    idle: 2,
    dnd: 3
  }
};

module.exports.presenceIds = new Set(Object.values(module.exports.presence));
