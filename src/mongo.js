const env = require("./env");
const utils = require("./utils");

const MongoDB = require("mongodb");

const BB = require("bluebird");
BB.promisifyAll(MongoDB);

MongoDB.connectAsync(`mongodb://${env.mongo.host}:${env.mongo.port}`, {
  useNewUrlParser: true
}).then(mongoConnection => {
  module.exports.db = mongoConnection.db(env.mongo.db);
  const mongo = module.exports;
  Promise.all([
    mongo.db.collection("subscriptions").deleteMany({
      hostAddress: env.hostAddress
    }),
    mongo.db
      .collection("sessions")
      .findAsync({
        hostAddress: env.hostAddress
      })
      .then(async sessionCursor => {
        const promises = new Map();
        const sessionDelete = mongo.db.collection("sessions").deleteMany({
          hostAddress: env.hostAddress
        });
        for await (const session of sessionCursor) {
          if (!promises.has(session.userId)) {
            promises.set(
              session.userId,
              mongo.db
                .collection("sessions")
                .findOneAsync({
                  userId: session.userId,
                  hostAddress: {$not: {$eq: env.hostAddress}},
                  sessionId: {$not: {$eq: session.sessionId}}
                })
                .then(
                  session =>
                    session ||
                    utils.setPresence({
                      userId: sessions.userId,
                      presence: constants.presence.offline,
                      activity: null
                    })
                )
            );
          }
        }

        await Promise.all([sessionDelete, Promise.all(promises.values())]);
      })
  ]);
});
