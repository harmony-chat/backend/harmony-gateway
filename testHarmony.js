const WebSocket = require("@discordjs/uws");
const msgpack = require("msgpack5")();
const ops = require("./src/ops");

const repl = require("repl");
const ws = new WebSocket("ws://localhost:3005");
ws.on("open", function() {
  ws.send(
    "6446686211837788160.WWOBTg==.w6pWwpnDqSd1w7nCg8KbwoJCwpdqwr7DrcO3w6rDqC59"
  );
});
ws.on("message", function(packet) {
  console.log(msgpack.decode(packet));
});

const r = repl.start();
r.context.ws = ws;
r.context.msgpack = msgpack;
r.context.send = function(op, data) {
  ws.send(msgpack.encode({o: ops[op], d: data}));
};
